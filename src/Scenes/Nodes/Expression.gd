extends BaseNode

onready var expression = get_node("LineEdit")
var last_expression: String = ""


func _ready():
	node_type = "Expression"


# Get a dictionary representing this node
# Separate from get_json for the purposes of saving
func get_data() -> Dictionary:
	var data = {
		"name": name,
		"type": "Expression",
		"offset": offset,
		"size": rect_size,
		"expression": expression.text,
		"next": "null"
	}
	
	return data


# Get a json string representing this node
func get_json() -> String:
	return to_json(get_data())


# Re-create this node from a json string
func from_json(data):
	.from_json(data)
	expression = get_node("LineEdit")
	
	var node_data = parse_json(data)
	
	expression.text = node_data["expression"]


func set_expression(new_expression: String):
	Globals.undo_redo.create_action("Change expression on %s" % name)
	Globals.undo_redo.add_do_property(expression, "text", new_expression)
	Globals.undo_redo.add_undo_property(expression, "text", last_expression)
	Globals.undo_redo.commit_action()
	
	last_expression = new_expression


func _on_LineEdit_text_entered(new_expression):
	set_expression(new_expression)


func _on_LineEdit_focus_exited():
	set_expression($LineEdit.text)


func _on_Expression_resize_request(new_minsize):
	rect_size.x = new_minsize.x
