extends BaseNode

onready var condition = get_node("Condition")
var undo = Globals.undo_redo

var last_condition: String = ""

func _ready():
	node_type = "Condition"

# Get a dictionary representing this node
# Separate from get_json for the purposes of saving
func get_data() -> Dictionary:
	var data = {
		"name": name,
		"type": "Condition",
		"offset": offset,
		"size": rect_size,
		"condition": condition.text,
		"next_true": "null",
		"next_false": "null"
	}
	
	return data

# Get a json string representing this node
func get_json() -> String:
	return to_json(get_data())

# Re-create this node from a json string
func from_json(data):
	.from_json(data)
	
	condition = get_node("Condition")
	
	var node_data = parse_json(data)
	
	condition.text = node_data["condition"]

func set_condition(new_condition):
	undo.create_action("Change condition on %s" % name)
	undo.add_do_property(condition, "text", new_condition)
	undo.add_undo_property(condition, "text", last_condition)
	undo.commit_action()
	
	last_condition = new_condition

func _on_Condition_text_entered(new_text):
	set_condition(new_text)

func _on_Condition_focus_exited():
	set_condition(condition.text)


func _on_Condition_resize_request(new_minsize):
	rect_size.x = new_minsize.x
