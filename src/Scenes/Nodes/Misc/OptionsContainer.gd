extends HSplitContainer

onready var option_edit = get_node("Option")
onready var condition_edit = get_node("Condition")

var last_option: String = ""
var last_condition: String = ""

func _ready():
	pass

func init(option: String, condition: String):
	option_edit = get_node("Option")
	condition_edit = get_node("Condition")
	option_edit.text = option
	condition_edit.text = condition

func get_option() -> String:
	return option_edit.text

func get_condition() -> String:
	return condition_edit.text


func get_split_offset() -> int:
	return split_offset


func set_option(option: String):
	Globals.undo_redo.create_action("Change option on %s" % get_parent().name)
	Globals.undo_redo.add_do_property(option_edit, "text", option)
	Globals.undo_redo.add_undo_property(option_edit, "text", last_option)
	Globals.undo_redo.commit_action()
	
	last_option = option

func set_condition(condition: String):
	Globals.undo_redo.create_action("Change condition on %s" % get_parent().name)
	Globals.undo_redo.add_do_property(condition_edit, "text", condition)
	Globals.undo_redo.add_undo_property(condition_edit, "text", condition)
	Globals.undo_redo.commit_action()
	
	last_condition = condition


func set_split_offset(new_split_offset: int):
	split_offset = new_split_offset


func update_conditions_visibility(show: bool):
	if show:
		condition_edit.show()
	else:
		condition_edit.hide()


func _on_Option_text_entered(new_text):
	set_option(new_text)


func _on_Option_focus_exited():
	set_option(option_edit.text)


func _on_Condition_text_entered(new_text):
	set_condition(new_text)


func _on_Condition_focus_exited():
	set_condition(condition_edit.text)

