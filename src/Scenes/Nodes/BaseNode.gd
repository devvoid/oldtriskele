class_name BaseNode

extends GraphNode

var node_type: String

func _ready():
	set_title(name)


func get_type() -> String:
	return node_type


func get_json() -> String:
	return ""


func get_binary_size() -> int:
	return 0


func get_binary() -> Array:
	return []

func from_json(data):
	var node_data = parse_json(data)
	name = node_data["name"]
	set_title(name)
	
	if node_data.has("size"):
		var node_size = node_data["size"].replace('(', '').replace(')', '').replace(' ', '').split(',')
		rect_size.x = float(node_size[0])
		rect_size.y = float(node_size[1])
	
	var node_offset = node_data["offset"].replace('(', '').replace(')', '').replace(' ', '').split(',')
	offset.x = float(node_offset[0])
	offset.y = float(node_offset[1])


func _on_node_dragged(from, to):
	Globals.undo_redo.create_action("Move node %s" % name)
	Globals.undo_redo.add_do_property(self, "offset", to)
	Globals.undo_redo.add_undo_property(self, "offset", from)
	Globals.undo_redo.commit_action()


func _on_close_request():
	Globals.undo_redo.create_action("Close node %s" % name)
	Globals.undo_redo.add_do_method(get_node('/root/MainScene'), "close_node", name)
	Globals.undo_redo.add_undo_method(get_node('/root/MainScene'), "recreate_node", get_json())
	Globals.undo_redo.commit_action()


func _on_resize_request(new_size):
	rect_size = new_size
