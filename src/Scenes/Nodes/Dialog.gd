extends BaseNode

# The previous input for Speaker/Dialog.
# Used for undoing text entry.
var last_dialog: String = ""
var last_speaker: String = ""

var undo = Globals.undo_redo

onready var speaker = get_node("Lines/Speaker")
onready var dialog = get_node("Lines/Dialog")

func _ready():
	node_type = "Dialog"
	_resize_dialog(rect_size)

# Get a dictionary representing this node
# Separate from get_json for the purposes of saving
func get_data() -> Dictionary:
	var data = {
		"name": name,
		"type": "Dialog",
		"offset": offset,
		"size": rect_size,
		"speaker": speaker.text,
		"dialog": dialog.text,
		"next": "null"
	}
	
	return data

# Get a json string representing this node
func get_json() -> String:
	return to_json(get_data())


func get_binary() -> Array:
	var node_length: int = 0
	return []


# Re-create this node from a json string
func from_json(data):
	.from_json(data)
	# I don't understand why these need to be gotten again after
	# re-creating the node.
	speaker = get_node("Lines/Speaker")
	dialog = get_node("Lines/Dialog")
	
	var node_data = parse_json(data)
	speaker.text = node_data["speaker"]
	dialog.text = node_data["dialog"]
	
	_resize_dialog(rect_size)

func set_speaker(new_speaker):
	undo.create_action("Change speaker on %s" % name)
	undo.add_do_property(speaker, "text", new_speaker)
	undo.add_undo_property(speaker, "text", last_speaker)
	undo.commit_action()
	
	last_speaker = new_speaker

func _on_Speaker_text_entered(new_speaker):
	set_speaker(new_speaker)

func _on_Speaker_focus_exited():
	set_speaker(speaker.text)

func set_dialog(new_dialog):
	undo.create_action("Change text on %s" % name)
	undo.add_do_property(dialog, "text", new_dialog)
	undo.add_undo_property(dialog, "text", last_dialog)
	undo.commit_action()
	
	last_dialog = new_dialog


func _on_Dialog_text_entered(new_dialog):
	set_dialog(new_dialog)


func _on_Dialog_focus_exited():
	set_dialog(dialog.text)


func _on_resize_request(new_size):
	._on_resize_request(new_size)
	_resize_dialog(new_size)


func _resize_dialog(new_size):
	$Lines.rect_min_size.y = new_size.y - 35
