extends BaseNode

onready var options_container = preload("res://Scenes/Nodes/Misc/OptionsContainer.tscn")

func _ready():
	node_type = "Options"


# Get a dictionary representing this node
# Separate from get_json for the purposes of saving
func get_data() -> Dictionary:
	var data = {
		"name": name,
		"type": "Options",
		"offset": offset,
		"size": rect_size,
		"use_conditions": $Buttons/UseConditions.pressed,
		# Each element must follow this pattern: {"option": "text", "condition": "", "split_offset": 0 next": "nextnode"}
		"options": []
	}
	
	for child in get_children():
		if child.name != "Buttons":
			var option_data = {
				"option": child.get_option(),
				"condition": child.get_condition(),
				"split_offset": child.get_split_offset(),
				"next": "null"
			}
			
			data["options"].append(option_data)
	
	return data


# Get a json string representing this node
func get_json() -> String:
	return to_json(get_data())


# Re-create this node from a json string
func from_json(data):
	.from_json(data)
	
	options_container = preload("res://Scenes/Nodes/Misc/OptionsContainer.tscn")
	
	var node_data = parse_json(data)
	
	# Compatability: 1.0.0beta6
	if node_data.has("use_conditions"):
		$Buttons/UseConditions.pressed = node_data["use_conditions"]
	
	for option in node_data["options"]:
		add_option(option)


func add_option(option: Dictionary):
	var child = options_container.instance()
	
	# Compatability: 1.0.0beta6
	if option.has("condition"):
		child.init(option["option"], option["condition"])
	else:
		child.init(option["option"], "")
	
	# Compatability: 1.0.0beta7
	if option.has("split_offset"):
		child.set_split_offset(option["split_offset"])
	
	child.update_conditions_visibility($Buttons/UseConditions.pressed)
	
	add_child(child, true)
	
	set_slot(get_child_count() - 1, false, 0, Color(1, 1, 1), true, 0, Color(1, 1, 1))


func remove_option():
	var child = get_child(get_child_count() - 1)
	if child.name != "Buttons":
		remove_child(child)
		child.queue_free()
	
	rect_size.y = 0


func _on_Add_pressed():
	var blank_option = {
		"option": "",
		"condition": ""
	}
	
	Globals.undo_redo.create_action("Add option to %s" % name)
	Globals.undo_redo.add_do_method(self, "add_option", blank_option)
	Globals.undo_redo.add_undo_method(self, "remove_option")
	Globals.undo_redo.commit_action()


func _on_Delete_pressed():
	var node = get_child(get_child_count() - 1)
	var old_node = {
		"option": node.get_option(),
		"condition": node.get_condition()
	}
	
	Globals.undo_redo.create_action("Add option to %s" % name)
	Globals.undo_redo.add_do_method(self, "remove_option")
	Globals.undo_redo.add_undo_method(self, "add_option", old_node)
	Globals.undo_redo.commit_action()


# TODO: Make this use UndoRedo.
# Currently I need to find a good way to use UndoRedo on
# a CheckBox. On its own, it doesn't update, but if I set
# pressed manually in the do/undo methods, I get some weird
# results.
func _on_UseConditions_toggled(button_pressed):
	for i in get_children():
		if i.name != "Buttons":
			i.update_conditions_visibility(button_pressed)
