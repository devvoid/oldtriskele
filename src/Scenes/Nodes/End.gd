extends BaseNode


func _ready():
	node_type = "End"


# Get a dictionary representing this node
# Separate from get_json for the purposes of saving
func get_data() -> Dictionary:
	var data = {
		"name": name,
		"type": "End",
		"offset": offset,
		"size": rect_size
	}
	
	return data


# Get a json string representing this node
func get_json() -> String:
	return to_json(get_data())


# Re-create this node from a json string
func from_json(data):
	.from_json(data)
