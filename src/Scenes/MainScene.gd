extends Control

# Current Triskele version.
const VERSION = "1.0.0beta9"

# Current TRIS format version
const BINARY_VERSION = 0

var NodeStart = preload("res://Scenes/Nodes/Start.tscn")

var NodeDialog = preload("res://Scenes/Nodes/Dialog.tscn")
var NodeExpression = preload("res://Scenes/Nodes/Expression.tscn")
var NodeOptions = preload("res://Scenes/Nodes/Options.tscn")
var NodeCondition = preload("res://Scenes/Nodes/Condition.tscn")

var NodeEnd = preload("res://Scenes/Nodes/End.tscn")

onready var graph = get_node("VBoxContainer/GraphEdit")
onready var path_label = get_node("VBoxContainer/MenuBar/FilePath")
onready var version_label = get_node("VBoxContainer/MenuBar/Version")
onready var undo = Globals.undo_redo

var current_file_path: String = ""

## Functions

func _ready():
	init_graph()
	
	var arguments = OS.get_cmdline_args()
	
	if arguments.size() >= 1:
		var arg = arguments[0]
		if arg.ends_with(".json"):
			open(arg)
	
	version_label.text = "Triskele %s" % VERSION


# Re-initialize the graph
# Called when the program is opened, or when New is selected in the file menu
func init_graph():
	clear_graph()
	
	# Add start/end nodes
	var start = NodeStart.instance()
	start.set_offset(Vector2(50, 300))
	graph.add_child(start)
	
	var end = NodeEnd.instance()
	end.set_offset(Vector2(500, 300))
	graph.add_child(end)


# Clear the graph of all children.
# Used in init_graph, and when opening a file.
func clear_graph():
	current_file_path = ""
	
	for child in graph.get_children():
		# Without these filters, clearing all children causes a crash
		if not("GraphEditFilter" in child.get_class()) and not ("Control" in child.get_class()):
			# Using free() directly is dangerous, but for some reason,
			# using queue_free() doesn't get rid of the Start and End nodes.
			child.free()


func _notification(what):
	if what == MainLoop.NOTIFICATION_WM_QUIT_REQUEST:
		$Quit.show()


# Create new node of the specified type
func create_node(id):
	var node_type
	
	match id:
		0:
			node_type = NodeDialog
		1:
			node_type = NodeExpression
		2:
			node_type = NodeOptions
		3:
			node_type = NodeCondition
		_:
			print("Node ID not recognized: %s" % id)
	
	var new_node = node_type.instance()
	new_node.set_offset(get_viewport().get_mouse_position() + graph.scroll_offset)
	graph.add_child(new_node, true)


# Close a node
# Used for redoing node deletion
func close_node(name: String):
	var node_to_delete = graph.get_node(name)
	
	var connection_list = graph.get_connection_list()
	
	for i in connection_list:
		if i["to"] == node_to_delete.name or i["from"] == node_to_delete.name:
			graph.disconnect_node(i["from"], i["from_port"], i["to"], i["to_port"])
	
	graph.remove_child(node_to_delete)
	node_to_delete.queue_free()


# Delete a node based on its index.
# This exists because the delete node method must be registered
# by the UndoRedo, but at that point, the node doesn't actually exist.
# All of this could be avoided if there were a way to register a do method
# on an UndoRedo without actually committing it right away, but oh well.
func close_node_by_index(id: int):
	var node_to_delete = graph.get_child(id).name
	close_node(node_to_delete)


# Recreate a node from its json data
# Used for undoing node deletion
func recreate_node(data: String):
	var node_data = parse_json(data)
	var node_type = get("Node"+node_data["type"])
	var new_node = node_type.instance()
	new_node.from_json(data)
	graph.add_child(new_node, true)


func save(file_path: String):
	# This dict will hold all our data, and be serialized at the end.
	var save_data = {
		"version": VERSION,
		"nodes": {}
	}
	
	# Get & save all nodes.
	for child in graph.get_children():
		if child.has_method("get_data"):
			save_data["nodes"][child.name] = child.get_data()
	
	# Godot's default connections list sucks, so it has to be sorted
	# into a more useful state first.
	var raw_connection_list = graph.get_connection_list()
	
	# This dict will hold all the connections in the following format:
	# "node_name": ["connection1", "connection2", ...]
	# This can be roughly translated into connect_node("node_name", array position, "other_node"[array position], 0)
	# The to_port is ignored because all nodes only have one input.
	var connection_list = {}
	
	for connection in raw_connection_list:
		# If this node doesn't have an entry in the list yet,
		# make it an empty array.
		if !connection_list.has(connection.from):
			connection_list[connection.from] = []
		
		# If the array is too small to contain the new connection, resize it
		# TODO: See if there's a way for variably-sized arrays in Godot
		if connection_list[connection.from].size() <= connection.from_port:
			connection_list[connection.from].resize(connection.from_port + 1)
		
		# Setup the connection
		connection_list[connection.from][connection.from_port] = connection.to
	
	# Finally, it's time to connect everything together.
	for key in connection_list.keys():
		# For Start, Dialog, and Expression, we just need to set
		# "next" to the first member of the connections array
		if save_data["nodes"][key].has("next"):
			save_data["nodes"][key]["next"] = connection_list[key][0]
		
		# For Condition, it's pretty simple too. Just set
		# "next_true" to array[0], and "next_false" to array[1]
		elif save_data["nodes"][key].has("next_true"):
			save_data["nodes"][key]["next_true"] = connection_list[key][0]
			save_data["nodes"][key]["next_false"] = connection_list[key][1]
		
		# Finally, for Options, we have to loop over every connection
		# in the list, and put it on the corresponding option.
		elif save_data["nodes"][key].has("options"):
			for i in connection_list[key].size():
				save_data["nodes"][key]["options"][i]["next"] = connection_list[key][i]
		
		# If none of those are true, node type is unknown.
		else:
			print("[ERROR]: Unknown node type!")
	
	# Open file & save.
	var file = File.new()
	file.open(file_path, File.WRITE)
	file.store_string(to_json(save_data))
	file.close()
	
	# Update the filepath label.
	current_file_path = file_path
	path_label.text = file_path.get_file()


# Export a TRIS binary file
func export_tris(file_path: String):
	# Get all nodes in a list
	var nodes = graph.get_children()
	
	# The graph has two nodes which aren't graph nodes; this removes them
	for i in nodes:
		if !i.is_in_group("trisnode"):
			nodes.erase(i)
	
	
	# Open file for writing
	var file = File.new()
	file.open(file_path, File.WRITE)
	
	# Write header
	file.store_buffer("TRIS".to_utf8())
	file.store_16(BINARY_VERSION)
	file.store_32(0)
	
	# Write offset list
	
	# Write nodes
	
	# Finish up and close
	file.close()


# Open a file
func open(file_path: String):
	# Clear the graph of any old nodes
	clear_graph()
	
	# Open file & load
	var file = File.new()
	file.open(file_path, File.READ)
	var json = file.get_as_text()
	file.close()
	
	# Parse & check version
	var save_data = parse_json(json)
	if save_data["version"] != VERSION:
		print("[ERROR]: Version mismatch in opened file! Errors may occur.")
	
	# Load the nodes
	var nodes = save_data["nodes"]
	for node_data in nodes.values():
		recreate_node(to_json(node_data))
	
	# Now that all the nodes are created, loop again and connect all nodes
	for node_data in nodes.values():
		# For Start, Dialog, and Expression, there's only one slot to worry about,
		# so just connect them.
		if node_data.has("next"):
			if node_data["next"] != "null":
				graph.connect_node(node_data["name"], 0, node_data["next"], 0)
		
		# On Condition, connect those two slots.
		if node_data.has("next_true"):
			if node_data["next_true"] != "null":
				graph.connect_node(node_data["name"], 0, node_data["next_true"], 0)
			
			if node_data["next_false"] != "null":
				graph.connect_node(node_data["name"], 1, node_data["next_false"], 0)
		
		# Options requires looping over all options to connect
		if node_data.has("options"):
			for i in node_data["options"].size():
				if node_data["options"][i]["next"] != "null":
					graph.connect_node(node_data["name"], i, node_data["options"][i]["next"], 0)
	
	# Update the filepath label.
	current_file_path = file_path
	path_label.text = file_path.get_file()

## Signals

func _on_MenuBar_file_menu_option_selected(id):
	match(id):
		0: # New
			$New.show()
		1: # Open
			$Open.show()
		3: # Save
			if current_file_path != "":
				save(current_file_path)
			else:
				$Save.show()
		4: # Save-As
			$Save.show()
		6: # Export
			$Export.show()
		8: # Quit
			$Quit.show()


func _on_MenuBar_edit_menu_option_selected(id):
	match id:
		0:
			$AddNodeMenu.rect_position = get_viewport().get_mouse_position() - Vector2(10, 10)
			$AddNodeMenu.show()
		1:
			undo.undo()
		2:
			undo.redo()
		_:
			print("[ERROR]: Unrecognized ID: %s" % id)


func _on_AddNodeMenu_id_pressed(id):
	undo.create_action("Create node %s" % id)
	undo.add_undo_method(self, "close_node_by_index", graph.get_child_count() - 1)
	undo.add_do_method(self, "create_node", id)
	undo.commit_action()


func _on_GraphEdit_connection_request(from, from_slot, to, to_slot):
	undo.create_action("Connect %s slot %s to %s slot %s" % [from, from_slot, to, to_slot])
	undo.add_do_method(graph, "connect_node", from, from_slot, to, to_slot)
	undo.add_undo_method(graph, "disconnect_node", from, from_slot, to, to_slot)
	undo.commit_action()


func _on_GraphEdit_disconnection_request(from, from_slot, to, to_slot):
	undo.create_action("Disconnect %s slot %s from %s slot %s" % [from, from_slot, to, to_slot])
	undo.add_do_method(graph, "disconnect_node", from, from_slot, to, to_slot)
	undo.add_undo_method(graph, "connect_node", from, from_slot, to, to_slot)
	undo.commit_action()


func _on_AddNodeMenu_focus_exited():
	$AddNodeMenu.hide()


func _on_Save_file_selected(path):
	save(path)


func _on_Export_file_selected(path):
	export_tris(path)


func _on_Open_file_selected(path):
	open(path)


func _on_AddNodeMenu_mouse_exited():
	$AddNodeMenu.hide()


func _on_New_confirmed():
	init_graph()


func _on_Quit_confirmed():
	get_tree().quit()
