extends HBoxContainer

signal file_menu_option_selected(id)
signal edit_menu_option_selected(id)


# Called when the node enters the scene tree for the first time.
func _ready():
	$File.get_popup().connect("id_pressed", self, "_on_file_menu_option_selected")
	$Edit.get_popup().connect("id_pressed", self, "_on_edit_menu_option_selected")
	
	# File
	var new_shortcut = ShortCut.new()
	new_shortcut.shortcut = ProjectSettings.get("input/new")["events"][0]
	$File.get_popup().set_item_shortcut(0, new_shortcut)
	
	var open_shortcut = ShortCut.new()
	open_shortcut.shortcut = ProjectSettings.get("input/open")["events"][0]
	$File.get_popup().set_item_shortcut(1, open_shortcut)
	
	var save_shortcut = ShortCut.new()
	save_shortcut.shortcut = ProjectSettings.get("input/save")["events"][0]
	$File.get_popup().set_item_shortcut(3, save_shortcut)
	
	var save_as_shortcut = ShortCut.new()
	save_as_shortcut.shortcut = ProjectSettings.get("input/save_as")["events"][0]
	$File.get_popup().set_item_shortcut(4, save_as_shortcut)
	
	var export_shortcut = ShortCut.new()
	export_shortcut.shortcut = ProjectSettings.get("input/export")["events"][0]
	$File.get_popup().set_item_shortcut(6, export_shortcut)
	
	var quit_shortcut = ShortCut.new()
	quit_shortcut.shortcut = ProjectSettings.get("input/quit")["events"][0]
	$File.get_popup().set_item_shortcut(8, quit_shortcut)
	
	# Edit
	var add_node_shortcut = ShortCut.new()
	add_node_shortcut.shortcut = ProjectSettings.get("input/add_node")["events"][0]
	$Edit.get_popup().set_item_shortcut(0, add_node_shortcut)
	
	var undo_shortcut = ShortCut.new()
	undo_shortcut.shortcut = ProjectSettings.get("input/undo")["events"][0]
	$Edit.get_popup().set_item_shortcut(1, undo_shortcut)
	
	var redo_shortcut = ShortCut.new()
	redo_shortcut.shortcut = ProjectSettings.get("input/redo")["events"][0]
	$Edit.get_popup().set_item_shortcut(2, redo_shortcut)


func _on_file_menu_option_selected(id):
	emit_signal("file_menu_option_selected", id)


func _on_edit_menu_option_selected(id):
	emit_signal("edit_menu_option_selected", id)
