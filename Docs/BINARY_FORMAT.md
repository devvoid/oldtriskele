# TRIS file format

TRIS is a binary file format intended for the storage of dialog trees.

TRIS is little-endian only. All text data is encoded in UTF-8.

## 1. The Header

The header stores some information about the file.

| Size               | Contents                   |
| ------------------ | -------------------------- |
| Five bytes; string | "TRIS\0" allcaps in UTF-8  |
| Two bytes; ushort  | TRIS format version number |
| Four bytes; uint   | Number of nodes.           |

The format version must be "0" currently.

## 2. Offsets

A list of offsets to all nodes in the file, stored in an ordered list.

Each offset is four bytes (uint), representing an offset to the node. Nodes are stored in sequential order, starting at 0 (the first node).

uint max is reserved to mean "no connection" - this is the end of the chain.

You can get a node by multiplying its index by 4, and then adding 10 (the header size), reading a uint from that offset, and seeking to that position.

The format is structured this way because not all nodes take up the same amount of data in memory, making this necessary to avoid needing to iterate over the file to find a specific node.

## 3. Nodes

After the offset is an array of nodes.

NOTE: All text data is encoded in UTF-8.

All nodes start with the same base information:

| Size                   | Contents    |
| ---------------------- | ------------|
| Two bytes; ushort      | Node type   |
| Two bytes; ushort      | Name length |
| Variable bytes; string | Name        |

Unless writing an editor or including debugging features, the name can be ignored.

The Node Type can be any of these values:
- 0: Dialog node. Holds dialog.
- 1: Expression node. Holds an expression to be run by the game code.
- 2: Options node. Holds a list of options the player can choose from.
- 3: Conditions node. Holds an expression to be run by the game code, and then branches based on the result.

Each node includes its own data, which is located immediately after the name ends.

### 3.1. Dialog

| Size                   | Contents         |
| ---------------------- | ---------------- |
| Four bytes; uint       | Speaker length   |
| Four bytes; uint       | Dialog length    |
| Four bytes; uint       | Next node        |
| Variable bytes; string | Speaker text     |
| Variable bytes; string | Dialog text      |

Both the speaker and dialog text should 

The speaker is intended to be the display name of the character; this can be ignored if the game doesn't need to display a speaker.

### 3.2. Expression

| Size                   | Contents          |
| ---------------------- | ----------------- |
| Four bytes; uint       | Expression length |
| Four bytes; uint       | Next node         |
| Variable bytes; string | Expression        |

### 3.3. Options node.

| Size              | Contents          |
| ----------------- | ----------------- |
| Two bytes; ushort | Number of options |

Afterwards is a list of options, which is formatted like so:

| Size                   | Contents          |
| ---------------------- | ----------------- |
| Four bytes; uint       | Option length     |
| Four bytes; uint       | Expression length |
| Four bytes; uint       | Next node         |
| Variable bytes; string | Option name       |
| Variable bytes; string | Expression        |

Expression must return a boolean; if false, the option is not selectable.

An empty expression must be interpreted as true.

### 3.4. Conditions node.

| Size                   | Contents           |
| ---------------------- | ------------------ |
| Four bytes; uint       | Expression length  |
| Four bytes; uint       | Next node if true  |
| Four bytes; uint       | Next node if false |
| Variable bytes; string | Expression         |

Expression must return a boolean.
