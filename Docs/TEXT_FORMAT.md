# File Format

Triskele JSON is formatted like so:

```json
{
	"version": "1.0.0",
	"nodes": {}
}
```

Each node has its own slightly different JSON layout. They're all contained within the "nodes" dictionary, using its name as the key.

Each node is guaranteed to have the following properties:

```json
{
	"name": "nodename",
	"type": "Type",
	"offset": "(0, 0)",
	"size": "(100, 100)"
}
```

`type` is the node type (Start, Dialog, Expression, Options, Condition, End). `offset` and `size` are the node's offset and size in the editor; unless you're writing a custom editor, it can be safely ignored.

All nodes (except for End) have their own variant of the `next` tag, which is the name of the next node. If there isn't another node after this (the result of nodes not being connected properly), this will be set to the string "null". If this happens, the parser must warn the user that the chain was broken.


## Start

```json
{
	"name": "nodename",
	"type": "Start",
	"offset": "(0, 0)",
	"size": "(100, 100)",
	"next": ""
}
```

## Dialog

```json
{
	"name": "nodename",
	"type": "Start",
	"offset": "(0, 0)",
	"size": "(100, 100)",
	"speaker": "",
	"dialog": "",
	"next": ""
}
```

`dialog` is the dialog to display. `speaker` is the name of the character currently speaking.

## Expression

```json
{
	"name": "nodename",
	"type": "Start",
	"offset": "(0, 0)",
	"size": "(100, 100)",
	"expression": ""
	"next": ""
}
```

`expression` is the expression to run.

## Options

```json
{
	"name": "nodename",
	"type": "Start",
	"offset": "(0, 0)",
	"size": "(100, 100)",
	"uses_conditions": false,
	"options": [
		{
			"option": "",
			"condition": "",
			"slider_offset": 0,
			"next": ""
		},
		...
	]
}
```

`options` is a list of options objects. Each option has four properties: `option`, the text of the option; `condition`, the condition that must be true for the option to be usable; `slider_offset`, the offset of the horizontal slider (which can be ignored unless you're making an editor); and `next`, the next node if this option is chosen.

If `uses_conditions` is false, `condition` should be ignored, and all options considered usable.

## Condition

```json
{
	"name": "nodename",
	"type": "Condition",
	"offset": "(0, 0)",
	"size": "(100, 100)",
	"expression": "",
	"next_true": "next node name",
	"next_false": "next node name"
}
```

`expression` is the expression to evaluate, which must return a boolean. Afterwards, `next_true` and `next_false` are the next nodes if the expression is true or false, respectively.

## End

```json
{
	"name": "nodename",
	"type": "End",
	"offset": "(0, 0)",
	"size": "(100, 100)"
}
```
