**NOTE: THIS REPO IS ABANDONED. Triskele is being rewritten from scratch.**

# Triskele

A dialog editor, made using the Godot Engine.

## Usage

Execution starts at the `Start` node, and continues from node to node, until eventually reaching the `End` node.

Each node's output (right socket) MUST only connect to ONE input (left socket). Godot's node editor doesn't have any way of enforcing this. If there are multiple of these connections, only the first connection made will count. Multiple outputs can connect to the same input without issue.

In other words, this is valid:

![Valid](Images/valid.png)

This is not:

![Not valid](Images/not_valid.png)

## Node types

### Dialog

![Dialog node](Images/node_dialog.png)

The Dialog node is the most common type of node, representing a block of dialog to be displayed. It has two fields: "Speaker", the name of the character speaking, and "Dialog", the dialog they're saying.

### Expression

![Expression node](Images/node_expression.png)

The Expression node contains a line of code to be run by the game.

### Options

![Options node](Images/node_options.png)

The Options node is the most complex, representing a list of options that the user can choose from. More options can be added or removed via the +/- buttons at the top of the node.

The checkbox enables the use of conditions. New lines will appear in addition to the already-existing ones. These are expressions, which must return booleans. If false, the option is disabled and cannot be selected by players.

### Condition

![Condition node](Images/node_condition.png)

The Condition node has an expression of its own, which must return a boolean, progressing to either True or False depending on its result.

## License

Triskele is provided under the MIT license.

Triskele's splash screen uses the [Conthrax](http://typodermicfonts.com/conthrax/) font, by Typodermic Fonts, Inc.

